using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public void OnClickPlayRestart()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void OnClickMenu()
    {
        SceneManager.LoadScene("MenuScene");
    } 
}
