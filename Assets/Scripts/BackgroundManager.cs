using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundManager : MonoBehaviour
{
    public float depth = 1;
    Player player;
    Vector2 velocity;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<Player>();
        velocity = player.GetComponent<Rigidbody2D>().velocity;
    }

    // Update is called once per frame
    void Update()
    {
        float realVelocity = velocity.x / depth;
        Vector2 backgroundPosition = transform.position;

        backgroundPosition.x -= realVelocity * Time.fixedDeltaTime;

        if (backgroundPosition.x <= -25)
            backgroundPosition.x = 80;

        transform.position = backgroundPosition;
    }
}
